import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * @author BRUNO NÚÑEZ MANUEL   1ºDAM DUAL B    07/06/2021
 */
public class Programilla {
    Scanner scanner = new Scanner(System.in);
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("personas.odb");
    EntityManager em = emf.createEntityManager();

    public static void main(String[] args) {
        new Programilla().run();
    }

    void run() {
        while (true) {
            try {
                System.out.println(
                        "\n1.Leer Fichero." +
                        "\n2.Pintar Matriz." +
                        "\n3.Crear Adulto." +
                        "\n4.Crear Adolescente." +
                        "\n5.Leer DB personas." +
                        "\n0.Salir.");
                switch (scanner.nextLine()) {
                    case "1" -> leerFichero();
                    case "2" -> pintarMatriz();
                    case "3" -> crearAdulto();
                    case "4" -> crearAdolescente();
                    case "5" -> leerDB();
                    case "0" -> salir();
                    default -> System.out.println("Elige una opción correcta.");
                }
            } catch (NumberFormatException e) {
                System.out.println("A lo mejor no has metido un número cuando debías truán...");
            }
        }
    }

    private void leerFichero() {
        BufferedReader reader = null;
        try {
            File file = new File("src/datos.txt");
            reader = new BufferedReader(new FileReader(file));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] words = line.split(" ");
                System.out.println(words.length + " " + line);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pintarMatriz() throws NumberFormatException {
        System.out.println("Introduce el número de filas: ");
        int filas = Integer.parseInt(scanner.nextLine());
        System.out.println("Introduce el número de columnas: ");
        int columnas = Integer.parseInt(scanner.nextLine());
        System.out.println("Introduce el caracter ASCII para pintar la matriz: ");
        char simbolo = scanner.nextLine().charAt(0);

        char[][] fxc = new char[filas][columnas];
        for (char[] chars : fxc) {
            Arrays.fill(chars, simbolo);
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (char[] chars : fxc) {
            stringBuilder.append("\n");
            for (char aChar : chars) {
                stringBuilder.append(aChar);
                stringBuilder.append(" ");
            }
        }

        System.out.println(stringBuilder);
    }

    private void crearAdulto() throws NumberFormatException {
        System.out.println("INICIANDO LA CREACIÓN DE UN ADULTO...");
        System.out.println("Inserta su nombre: ");
        String nombre = scanner.nextLine();
        System.out.println("Inserta su edad: ");
        int edad = Integer.parseInt(scanner.nextLine());
        System.out.println("Inserta su trabajo: ");
        String trabajo = scanner.nextLine();
        Adulto adulto = new Adulto(nombre, edad, trabajo);
        em.getTransaction().begin();
        em.persist(adulto);
        em.getTransaction().commit();
    }

    private void crearAdolescente() throws NumberFormatException {
        System.out.println("INICIANDO LA CREACIÓN DE UN ADOLESCENTE...");
        System.out.println("Inserta su nombre: ");
        String nombre = scanner.nextLine();
        System.out.println("Inserta su edad: ");
        int edad = Integer.parseInt(scanner.nextLine());
        System.out.println("Inserta su curso: ");
        String curso = scanner.nextLine();
        Adolescente adolescente = new Adolescente(nombre, edad, curso);
        em.getTransaction().begin();
        em.persist(adolescente);
        em.getTransaction().commit();
    }

    private void leerDB() {
        TypedQuery<Adulto> query = em.createQuery("SELECT a FROM Adulto a", Adulto.class);
        List<Adulto> results = query.getResultList();
        for (Adulto a : results) {
            System.out.println(a);
        }
        TypedQuery<Adolescente> query2 = em.createQuery("SELECT ad FROM Adolescente ad", Adolescente.class);
        List<Adolescente> results2 = query2.getResultList();
        for (Adolescente a : results2) {
            System.out.println(a);
        }
    }

    private void salir() { // Totalmente innecesario pero queda cool.
        em.close();
        emf.close();
        System.exit(0);
    }

}
