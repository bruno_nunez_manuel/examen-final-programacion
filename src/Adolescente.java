import javax.persistence.Entity;

@Entity
public class Adolescente extends Persona {
    String curso;

    public Adolescente(String nombre, int edad, String curso) {
        super(nombre, edad);
        this.curso = curso;
    }

    public String estudiar() {
        return this.getNombre() + " estudia.";
    }

    @Override
    public String toString() {
        return "Adolescente{" +
                "id= " + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", edad=" + getEdad() +
                ", curso='" + curso + '\'' +
                '}';
    }
}
