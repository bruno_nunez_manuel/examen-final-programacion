import javax.persistence.Entity;

@Entity
public class Adulto extends Persona implements Juego {
    String trabajo;

    public Adulto(String nombre, int edad, String trabajo) {
        super(nombre, edad);
        this.trabajo = trabajo;
    }

    public String trabajar(int horas) {
        return this.getNombre() + " trabaja.";
    }

    @Override
    public String jugar() {
        return this.getNombre() + " juega.";
    }

    @Override
    public String toString() {
        return "Adulto{" +
                "id= " + getId() + '\'' +
                ", nombre='" + getNombre() + '\'' +
                ", edad=" + getEdad() +
                ", trabajo='" + trabajo + '\'' +
                '}';
    }
}
